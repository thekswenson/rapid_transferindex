#Rapid Transfer Index#

This is python code to compute the Transfer Index between two trees in quasilinear time. The algorithm is described in [2]. All input files have to be in Newick format.

##Usage##

`python ./transfer.py newick_tree_1 newick_tree_2`

Computes the transfer index between all branches of newick_tree_1 and newick_tree_2. Outputs a tree whose topology is the same as in newick_tree_1, but whose branch lengths correspond to transfer index values for each branch. 

`python ./transfer.py reference_tree bootstrap_file boot`

Computes the transfer bootstrap expectation (TBE) values for each branch in reference_tree based on the trees in bootstrap_file (one newick tree per line) as proposed in Lemoine et al.[1].

`python ./transfer.py newick_tree_1 newick_tree_2 naive`

Computes the transfer index using a naive, O(n^3)-time algorithm that enumerates all taxa on each side of every branch in T and T^* and counts the number of differences. Very slow, but useful for testing. 

##Test Code##

Test code resides in the `tests/` directory. To compare running times between the new method and the old booster code use the `tests/time/runTimeTests.sh` script after installing booster in the top directory of this repository. Get booster from [https://github.com/thekswenson/booster/tree/WABI](https://github.com/thekswenson/booster/tree/WABI) .

##Contacts##

jakub.truszkowski@gmail.com
swenson@lirmm.fr

##References##

[1] Lemoine, F., Entfellner, J. B. D., Wilkinson, E., Correia, D., Felipe, M. D., Oliveira, T. D., & Gascuel, O. (2018). Renewing Felsenstein's phylogenetic bootstrap in the era of big data. Nature, 556(7702), 452.

[2] Truszkowski, J., Gascuel, O., Swenson, K.M. (2019). Rapidly computing the phylogenetic transfer index.  In 19th International Workshop on Algorithms in Bioinformatics.

from math import log,exp,ceil
import random
import sys

def make_comp_list(lst):

	total_size=int(2**(ceil(log(len(lst),2)))*2)
	aux_elem_size=total_size/2
	comp_list=[0 for i in range(aux_elem_size)]
	comp_list.extend(lst)
	null_val=-1000000000
	comp_list.extend([null_val for i in range(len(comp_list),total_size)])

	for i in range(aux_elem_size-1,0,-1):#1 is the index of the global max
		leftchild=2*i
		rightchild=leftchild+1
		comp_list[i]=max(comp_list[leftchild],comp_list[rightchild])
	return comp_list

def update_comp_list(comp_list,lst_index,val):
	
	internal_ind=len(comp_list)/2+lst_index
	comp_list[internal_ind]=val
	while internal_ind>1:
		internal_ind/=2
		leftchild=internal_ind*2
		rightchild=leftchild+1
		newval=max(comp_list[leftchild],comp_list[rightchild])
		if newval==comp_list[internal_ind]:
			break
		comp_list[internal_ind]=newval
	return comp_list[1]


def test(listlen,nupdates):
	testlist=[random.randint(0,1000000) for i in range(listlen)]
	comp_list=make_comp_list(testlist)

	for i in range(nupdates):
		newval=random.randint(0,1000000)
		newpos=random.randint(0,len(testlist)-1)
		testlist[newpos]=newval
		newmax=update_comp_list(comp_list,newpos,newval)
		assert newmax==max(testlist)

def adv_test(listlen,nupdates):
	testlist=[random.randint(0,1000000) for i in range(listlen)]
	comp_list=make_comp_list(testlist)

	for i in range(nupdates):
		newpos=testlist.index(max(testlist))
		newval=testlist[newpos]-10000
		testlist[newpos]=newval
		newmax=update_comp_list(comp_list,newpos,newval)
		assert newmax==max(testlist)

def speed_test(listlen,nupdates):
	testlist=[random.randint(0,1000000) for i in range(listlen)]
	comp_list=make_comp_list(testlist)

	for i in range(nupdates):
		newval=random.randint(0,1000000)
		newpos=random.randint(0,len(testlist)-1)
		testlist[newpos]=newval
		newmax=update_comp_list(comp_list,newpos,newval)

def speed_test_deterministic(listlen,nupdates):
	testlist=[i for i in range(listlen)]
	comp_list=make_comp_list(testlist)

	for i in range(nupdates):
		newpos=i%listlen
		newval=testlist[newpos]-100000000
		testlist[newpos]=newval
		newmax=update_comp_list(comp_list,newpos,newval)

adv_test(1000,1000)
test(10000,10000)
#speed_test(1000000,1000000)
#speed_test_deterministic(1000000,1000000)
print "Completed!"



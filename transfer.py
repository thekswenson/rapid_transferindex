import dendropy
import sys
from collections import deque
import sets
import copy
from math import ceil,log,exp
import re

from dendropy.simulate import treesim #for tests only

#a string for fake taxon names - needed for pruning identical subtrees
shrink_subtrees=True
arttaxstr='ArtTaxon'

pardict={}
leftdict={}
rightdict={}
minvaldict={}
maxvaldict={}
ddict={}
counter=0
parent_node={}
taxon_id={}

path_num={}

min_glob=1000000000
max_glob=-1000000000
comp_list=[]
comp_list_zero=[]


def reset_extrema():
	global min_glob,max_glob,comp_list_zero,comp_list
	min_glob=1000000000
	max_glob=-1000000000
	comp_list=comp_list_zero # reset the data structure without building from scratch


def count_desc_leaves(T):
	for node in T.postorder_node_iter():
		if node.is_leaf():
			node.nleaves=1
		else:
			node.nleaves=sum([nd.nleaves for nd in node.child_nodes()])
	return T.seed_node.nleaves


def find_max_child(node):
	maxnl=0
	mchild=None
	others=[]
	for child in node.child_nodes():
		if child.nleaves>maxnl:
			maxnl=child.nleaves
			if mchild is not None:
				others.append(mchild)
			mchild=child
		else:
			others.append(child)
	assert mchild!=None
	return mchild,others



# returns a list of heavy paths
def get_heavy_paths(T):
	paths=[]
	q=deque()
	q.append(T.seed_node)
	
	while q:
		curnode=q.popleft()
		path=[curnode]
		assert curnode is not None
		while not curnode.is_leaf():
			mchild,others=find_max_child(curnode)
			assert mchild is not None
			q.extend(others)
			curnode=mchild
			path.append(curnode)
		paths.append(path)

	return paths

def assign_ids_to_nodes(T):
	cnt=0
	for node in T.preorder_node_iter():
		node.id=cnt
		cnt=cnt+1

def pick_midpoint(path,aind,bind):
	assert aind<bind
	return aind+(bind-aind)/2





def set_up_path_search_tree_simple(path):
	global pardict,leftdict,rightdict,minvaldict,maxvaldict,ddict

	pathids=map(lambda x:x.id,path)
	pathleafnums=map(lambda x:x.nleaves,path)
	q=deque()
	root_int_index=(0,len(path)-1)
	pardict[(pathids[0],pathids[-1])]=None
	q.append(root_int_index)
	while q:
		(aind,bind)=q.popleft()
		a=pathids[aind]; b=pathids[bind]
		curint=(a,b)
		minvaldict[curint]=pathleafnums[bind]
		maxvaldict[curint]=pathleafnums[aind]

		if a==b:
			ddict[curint]=pathleafnums[bind]
		else:
			ddict[curint]=0

			mid=pick_midpoint(path,aind,bind)
			leftint=(a,pathids[mid])
			q.append((aind,mid))
			rightint=(pathids[mid+1],b)
			q.append((mid+1,bind))
			pardict[leftint]=curint
			pardict[rightint]=curint
			leftdict[curint]=leftint
			rightdict[curint]=rightint

#### end of set_up_path_search_tree_simple

def set_up_parents(T):
	global parent_node
	for node in T.postorder_node_iter():
		if node.parent_node is not None:
			parent_node[node.id]=node.parent_node.id
		else:
			parent_node[node.id]=None

def set_up_taxon_ids(T):
	global taxon_id
	for leaf in T.leaf_nodes():
		taxon_id[leaf.taxon.label]=leaf.id

def copy_taxon_ids(T):
	global taxon_id
	for leaf in T.leaf_nodes():
		leaf.id=taxon_id[leaf.taxon.label]

def set_up_visit_marks(T):
	for node in T.postorder_node_iter():
		node.visited=False

def set_up_everything(T):
	global parent_node,counter,path_num,maxvaldict,comp_list,comp_list_zero
	#count_desc_leaves(T) #well, this has to be done separately...
	assign_ids_to_nodes(T)
	set_up_parents(T)

	set_up_taxon_ids(T)
	

	paths=get_heavy_paths(T)
	maxima=[0 for i in range(len(paths))]
	for i in range(len(paths)):
		set_up_path_search_tree_simple(paths[i])
		#now set up the data structure for maxima
		root_int=(paths[i][0].id,paths[i][-1].id)
		path_num[root_int]=i
		maxima[i]=maxvaldict[root_int]

	#now create the data structure
	comp_list=make_comp_list(maxima)
	comp_list_zero=comp_list


def update_path(nodeid,d):
	global ddict,minvaldict,maxvaldict,pardict,leftdict
	global min_glob,comp_list,path_num

	cur_int=(nodeid,nodeid)
	ddict[cur_int]+=d
	minvaldict[cur_int]+=d
	maxvaldict[cur_int]+=d

	while pardict[cur_int] is not None:
		par_int=pardict[cur_int]
		left_int=leftdict[par_int]
		right_int=rightdict[par_int]
		if par_int[1]==cur_int[1]:
			ddict[left_int]+=d
			minvaldict[left_int]+=d
			maxvaldict[left_int]+=d
		minvaldict[par_int]=min(minvaldict[left_int],minvaldict[right_int])+ddict[par_int]
		maxvaldict[par_int]=max(maxvaldict[left_int],maxvaldict[right_int])+ddict[par_int]
		cur_int=par_int
	if minvaldict[cur_int]<min_glob:
		min_glob=minvaldict[cur_int]
	#if maxvaldict[cur_int]>max_glob:
	#	max_glob=maxvaldict[cur_int]
	update_comp_list(comp_list,path_num[cur_int],maxvaldict[cur_int])

	return cur_int

#adds (direction=+1) or deletes (direction=-1) the leaf from the working set
def change_leaf_status(nodeid,direction):
	global counter,parent_node
	counter+=direction
	(first,last)=update_path(nodeid,-2*direction)
	while first!=0:
		x=parent_node[first]
		(first,last)=update_path(x,-2*direction)

def get_leaf_id(leaf):
	tlabel=leaf.taxon.label
	tid=taxon_id[tlabel]
	return tid

def get_sibling(node):
	parent=node.parent_node
	for nd in parent.child_nodes():
		if nd!=node:
			return nd


def compute_transfer_inds(T,Tother):
	global counter
	global shrink_subtrees
	T.resolve_polytomies()
	Tother.resolve_polytomies()
	nleaves=count_desc_leaves(T)
	count_desc_leaves(Tother)
	if shrink_subtrees:
		nodesT,nodesT2=find_max_ident_subtrees(T,Tother)
		if len(nodesT)<=1:
			#assert len(nodesT2)
			for node in T.postorder_node_iter():
				node.dist=0
				return
		removedT=detach_subtrees(T,nodesT)
		removedT2=detach_subtrees(Tother,nodesT2)
	else:
		for leaf in T.leaf_nodes():
			leaf.weight=1
	set_up_everything(Tother)
	copy_taxon_ids(T)
	set_up_visit_marks(T)
	#display_param(Tother,invariant)
	for leaf in T.leaf_nodes():
		leaf.dist=0 # there's always a leaf like that in the other tree
		node=leaf
		#print "adding as first:"+str(node.taxon.label)
		change_leaf_status(node.id,node.weight)
		#display_param(Tother,invariant)
		while node.parent_node is not None:
			#for now, let's just do binary trees
			parent=node.parent_node
			if node.nleaves*2>=parent.nleaves and not parent.visited:
				sibling=get_sibling(node)
				for sideleaf in sibling.leaf_nodes():
					#print "adding:"+str(sideleaf.taxon.label)
					change_leaf_status(sideleaf.id,sideleaf.weight)

				max_glob=comp_list[1]

				mindist=min(min_glob+counter,nleaves-max_glob-counter)
				parent.dist=mindist
				parent.visited=True
				node=parent
			else:
				#print "REMOVING LEAVES:"
				for descleaf in node.leaf_nodes():
					change_leaf_status(descleaf.id,-descleaf.weight)
					#print "removing:"+str(descleaf.taxon.label)
					#display_param(Tother,invariant)
				reset_extrema()
				break
			#display_param(Tother,invariant)

		if node.parent_node is None: # reached the root, must clean up everything
			for descleaf in node.leaf_nodes():
				change_leaf_status(descleaf.id,-descleaf.weight)
				#print "removing:"+str(descleaf.taxon.label)
				#display_param(Tother,invariant)
			reset_extrema()
	#end of main loop
	if shrink_subtrees:
		reattach_subtrees(T,nodesT,removedT)
		reattach_subtrees(Tother,nodesT2,removedT2)






def compute_transfer_inds_naive(T,Tother):
	count_desc_leaves(Tother)
	set_up_everything(Tother)
	nleaves=count_desc_leaves(T)
	copy_taxon_ids(T)
	wholeset=sets.Set(map(lambda x:x.id,T.leaf_nodes()))
	for node in T.postorder_node_iter():
		curset=sets.Set(map(lambda x:x.id,node.leaf_nodes()))
		mindist=10000000

		for node2 in Tother.postorder_node_iter():
			curset2=sets.Set(map(lambda x:x.id,node2.leaf_nodes()))
			dist1=len(curset^curset2)
			dist2=len((wholeset-curset)^curset2)
			dist=min(dist1,dist2)
			if dist<mindist:
				mindist=dist

		node.naivedist=mindist


# collapsing identical subtrees to speed up execution

def get_pseudoleaves(nd):
	return [node for node in nd.postorder_iter(lambda x: x.pseudoleaf)]

def find_maximal_identical_subtrees(T,Tother):
	t1nodedict={}
	nodesfirst=[]
	nodessecond=[]
	for node in T.postorder_node_iter():
		node.pseudoleaf=False
		if node.is_leaf():
			taxstr=str(node.taxon.label)
			node.tup=(taxstr,taxstr)
			t1nodedict[node.tup]=node
			node.dist=0
		else:
			tup=tuple(min(child.tup) for child in node.child_nodes())
			node.tup=tup
			t1nodedict[tup]=node
			node.dist=0 # will be overwritten later anyway

	for node in Tother.postorder_node_iter():
		node.pseudoleaf=False
		if node.is_leaf():
			taxstr=str(node.taxon.label)
			node.tup=(taxstr,taxstr)
			node.has_exact_match=True
			node.dist=0
		else:
			tup=tuple(min(child.tup) for child in node.child_nodes())
			node.tup=tup
			if tup in t1nodedict and all([child.has_exact_match for child in node.child_nodes()]):
				node.has_exact_match=True
				node.dist=0
			else:
				node.has_exact_match=False
				for child in node.child_nodes():
					if child.has_exact_match:
						nodessecond.append(child)
						child.pseudoleaf=True
						nodesfirst.append(t1nodedict[child.tup])
						t1nodedict[child.tup].pseudoleaf=True

	T.pseudoleaves=nodesfirst
	Tother.pseudoleaves=nodessecond
	return nodesfirst,nodessecond					

def find_max_ident_subtrees(T,Tother):
	leaves_by_taxon={}
	nodesfirst=[]
	nodessecond=[]
	for leaf in T.leaf_nodes():
		leaves_by_taxon[leaf.taxon.label]=leaf

	for node in T.postorder_node_iter():
		node.pseudoleaf=False
		node.dist=0

	for node in Tother.postorder_node_iter():
		node.pseudoleaf=False
		node.dist=0

	for leaf in Tother.leaf_nodes():
		leaf.other=leaves_by_taxon[leaf.taxon.label]
		leaf.has_exact_match=True
		leaf.dist=0

	for node in Tother.postorder_node_iter():
		if node.is_leaf():
			continue
		if all([child.has_exact_match for child in node.child_nodes()]):
			otherparents=[child.other.parent_node for child in node.child_nodes()]
			if all([par==otherparents[0] for par in otherparents]):
				node.has_exact_match=True
				node.dist=0
				node.other=otherparents[0]
			else:
				#node is not a repeated subtree but all of its children are
				node.has_exact_match=False
				for child in node.child_nodes():
					nodesfirst.append(child)
					nodessecond.append(child.other)
					child.pseudoleaf=True
					child.other.pseudoleaf=True

		else:
			#one of the children doesn't have a matching subtree so just mark False
			node.has_exact_match=False
			for child in node.child_nodes():
					if not child.has_exact_match:
						continue
					nodesfirst.append(child)
					nodessecond.append(child.other)
					child.pseudoleaf=True
					child.other.pseudoleaf=True

	T.pseudoleaves=nodesfirst
	Tother.pseudoleaves=nodessecond
	return nodesfirst,nodessecond
		

def detach_subtrees(T,subroots):
	removed=[]
	for i in range(len(subroots)):
		node=subroots[i]
		node.weight=len(node.leaf_nodes())
		children=[]
		for child in node.child_nodes():
			children.append(child)
			node.remove_child(child)
		removed.append(children)
		if len(children)>0:
			node.taxon=dendropy.Taxon(label=arttaxstr+str(i))

	return removed

def reattach_subtrees(T,subroots,removed):
	assert len(subroots)==len(removed)
	for i in range(len(subroots)):
		for child in removed[i]:
			subroots[i].add_child(child)
		if not subroots[i].is_leaf():
			subroots[i].taxon=None




def dists_as_blengths(T):
	for node in T.postorder_node_iter():
		node.edge_length=node.naivedist

def dists_as_blengths2(T):
	for node in T.postorder_node_iter():
		node.edge_length=node.dist

def invariant(node):
	global pardict
	global ddict
	x=node.id
	cur_int=(x,x)
	acc=ddict[cur_int]
	while pardict[cur_int] is not None:
		cur_int=pardict[cur_int]
		acc+=ddict[cur_int]
	return acc+counter


def display_param(T,func):
	Tdisplay=copy.deepcopy(T)
	for node in Tdisplay.postorder_node_iter():
		node.edge_length=func(node)
	print Tdisplay


#-----------Data structure for maintaining max in a fixed-size but mutable set


def make_comp_list(lst):

	total_size=int(2**(ceil(log(len(lst),2)))*2)
	aux_elem_size=total_size/2
	comp_list=[0 for i in range(aux_elem_size)]
	comp_list.extend(lst)
	null_val=-1000000000
	comp_list.extend([null_val for i in range(len(comp_list),total_size)])

	for i in range(aux_elem_size-1,0,-1):#1 is the index of the global max
		leftchild=2*i
		rightchild=leftchild+1
		comp_list[i]=max(comp_list[leftchild],comp_list[rightchild])
	return comp_list

def update_comp_list(comp_list,lst_index,val):
	
	internal_ind=len(comp_list)/2+lst_index
	comp_list[internal_ind]=val
	while internal_ind>1:
		internal_ind/=2
		leftchild=internal_ind*2
		rightchild=leftchild+1
		newval=max(comp_list[leftchild],comp_list[rightchild])
		if newval==comp_list[internal_ind]:
			break
		comp_list[internal_ind]=newval
	return comp_list[1]

#------End of data structure

#------Transfer bootstrap expectation (TBE)------

def add_TS(reftree,btree):
	compute_transfer_inds(reftree,btree)

	for node in reftree.postorder_node_iter():
		node.indsum+=node.dist

def compute_TBE(reftree,bstrings):
	reftree.resolve_polytomies()
	for node in reftree.postorder_node_iter():
		node.indsum=0.0

	tnum=0
	for bstring in bstrings:
		print >>sys.stderr,"Processing tree #:"+str(tnum)
		btree=dendropy.Tree.get_from_string(bstring,schema='newick',case_sensitive_taxon_labels=True,taxon_namespace=reftree.taxon_namespace)
		add_TS(reftree,btree)
		tnum+=1

	nboot=len(bstrings)
	totalleaves=count_desc_leaves(reftree)
	for node in reftree.postorder_node_iter():
		node.indsum=node.indsum/nboot # get the average transfer index
		if node.nleaves==1 or node.nleaves==totalleaves-1 or node.parent_node is None:
			node.support=1.0
		else:
			node.support=1.0-node.indsum/(min(node.nleaves,totalleaves-node.nleaves)-1)

def display_support_values(reftree):
	for node in reftree.postorder_node_iter():
		if not node.is_leaf() and not node.parent_node is None:
			node.label=str(node.support)


def read_bootstrap_trees(bpath):
	btreelist=dendropy.TreeList.get(path=bpath,schema='newick')
	return btreelist

#returns list of newick strings to be parsed one by one 
def read_boostrap_trees_long(bpath):
	F=file(bpath,"r")
	wholestr=F.read()
	treestraw=wholestr.split(';')
	treesttrimmed=filter(lambda x:x[0]=='(',map(lambda x:x.strip()+';',treestraw))
	return treesttrimmed

#inserts special chars to disambiguate lower- and uppercase after they're conflated

def fix_uppercase(treestr):
	upcase_mark='wxyzab'
	modst=re.sub('[A-Z]',lambda x:x.group()+upcase_mark,treestr)
	return modst

def undo_fix_uppercase(modst):
	upcase_mark='wxyzab'
	treest=re.sub(upcase_mark,'',modst)
	return treest





#------End of TBE

#----TESTS----

def verify_dist(T):

	for node in T.postorder_node_iter():
		assert node.dist==node.naivedist

def test_rand_trees(treesize):
	T=treesim.birth_death_tree(1.0,0.0,ntax=treesize)
	T2=treesim.birth_death_tree(1.0,0.0,taxon_namespace=T.taxon_namespace)
	compute_transfer_inds(T,T2)
	compute_transfer_inds_naive(T,T2)

	verify_dist(T)

def test_same_trees(treesize):
	T=treesim.birth_death_tree(1.0,0.0,ntax=treesize)
	T2=copy.deepcopy(T)
	compute_transfer_inds(T,T2)
	compute_transfer_inds_naive(T,T2)

	verify_dist(T)

def random_tree_test(treesize,ntests):
	for i in range(ntests):
		print "two random trees, test:"+str(i)
		test_rand_trees(treesize)

def random_tree_speed_test(treesize,ntests):
	for i in range(ntests):
		if i%10==0:
			print "two random trees, testing speed:"+str(i)
		T=treesim.birth_death_tree(1.0,0.0,ntax=treesize)
		T2=treesim.birth_death_tree(1.0,0.0,taxon_namespace=T.taxon_namespace)
		compute_transfer_inds(T,T2)

def same_tree_test(treesize,ntests):
	for i in range(ntests):
		print "two equal trees, test:"+str(i)
		test_same_trees(treesize)




sys.setrecursionlimit(20000)
tns=dendropy.TaxonNamespace(is_case_sensitive=True)
T=dendropy.Tree.get_from_path(sys.argv[1],schema='newick',case_sensitive_taxon_labels=True,taxon_namespace=tns)

naive=False
boot=False
if len(sys.argv)>3 and sys.argv[3]=='naive':
	naive=True
elif len(sys.argv)>3 and sys.argv[3]=='boot':
	boot=True

if not naive and not boot:
	T2=dendropy.Tree.get_from_path(sys.argv[2],schema='newick',case_sensitive_taxon_labels=True,taxon_namespace=T.taxon_namespace)
	
	compute_transfer_inds(T,T2)

	dists_as_blengths2(T)
	Tdisplay=T
elif naive:

	T2=dendropy.Tree.get_from_path(sys.argv[2],schema='newick',case_sensitive_taxon_labels=True,taxon_namespace=T.taxon_namespace)
	compute_transfer_inds_naive(T,T2)
	Tdisplay=copy.deepcopy(T)
	dists_as_blengths(Tdisplay)
else:
	bstrings=read_boostrap_trees_long(sys.argv[2])

	compute_TBE(T,bstrings)
	Tdisplay=T
	display_support_values(Tdisplay)
	Tdisplay.collapse_basal_bifurcation()

print str(Tdisplay)+';'
#!/bin/bash


for ntaxa in 20 50 100 200
do
	for i in `seq 0 9`
	do
		nops=10
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		#echo $prefix'.comp'
		python ../generateTree.py $ntaxa $nops $prefix
		python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp'
		python ../../transfer.py $prefix'_1' $prefix'_2' naive> $prefix'.compnaive'
		if cmp $prefix'.comp' $prefix'.compnaive' 
		then
			echo 'test:'$prefix': OK'
		else
			echo 'test:'$prefix': WRONG!!!'
		fi
	done
done

#now vary nops

for nops in 0 10 20 50
do
	for i in `seq 0 9`
	do
		ntaxa=100
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		#echo $prefix'.comp'
		python ../generateTree.py $ntaxa $nops $prefix
		python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp'
		python ../../transfer.py $prefix'_1' $prefix'_2' naive> $prefix'.compnaive'
		if cmp $prefix'.comp' $prefix'.compnaive' 
		then
			echo 'test:'$prefix': OK'
		else
			echo 'test:'$prefix': WRONG!!!'
		fi
	done
done
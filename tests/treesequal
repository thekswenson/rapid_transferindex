#!/usr/bin/python3
"""
Limit the first tree to the leaf set from the second, and then test is the tree
topologies and branch labels match.
"""
import sys
import os
import argparse

import dendropy
from dendropy import Tree, Annotation, Node, Taxon
from dendropy.calculate import treecompare

sys.setrecursionlimit(1500)   #Change this in case we need to read LARGE trees.


  #CONSTANTS:

THRESHOLD = 0.00001   #Threshold under which support values are condered equal.

LEAFSET = 'leafset'
VISITED = 'visited'

#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def labelNodesWithLeafSets(tree: Tree) -> None:
  """
  Label nodes with their sets of pendant leaves.
  
  Parameters
  ----------
  tree1 : Tree
      A dendropy Tree object
  """
  for n in tree.postorder_node_iter():
    if n.is_leaf():
      n.leafset = set((n.taxon, ))
      n.visited = False

    else:
      n.leafset = set()
      for child_node in n.child_node_iter():
        n.leafset |= child_node.leafset

      n.visited = False


def verifyEqual(t1: Tree, t2: Tree) -> bool:
  """
  Verify that the two trees are equal and their support values match.
  
  Parameters
  ----------
  t1 : Tree
      first tree
  t2 : Tree
      second tree

  Returns
  ----------
  True if the trees match
  """
    # Follow each leaf to the root in both trees as long as the attribute
    # 'visited' is False:

    #Root tree at random taxon:
  rerootAndCheckCherry(t1, t2)

    #Initialize the LEAFSET and VISITED attributes of edges:
  labelNodesWithLeafSets(t1)
  labelNodesWithLeafSets(t2)

    #Check paths to root (that have not been visited):
  for leaf1 in t1.leaf_node_iter():
    leaf2 = t2.find_node_for_taxon(leaf1.taxon)
    if not checkPathToRoot(leaf1, leaf2, t1.seed_node, t2.seed_node):
      return False

  return True


def rerootAndCheckCherry(t1: Tree, t2: Tree) -> Taxon:
  """
  Reroot the given trees and check that the support value on the cherry matches.
  
  Parameters
  ----------
  t1 : Tree
      first tree
  t2 : Tree
      second tree
  """
  leaf1 = next(t1.leaf_node_iter())
  roottaxon = leaf1.taxon
  leaf2 = t2.find_node_for_taxon(roottaxon)

    #Check the cherry.
  if(leaf1.parent_node.label and leaf2.parent_node.label and
     not equalSupport(leaf1.parent_node, leaf2.parent_node)):
    print(f'Mismatch in support for parent nodes of:\n'
          f'First tree leaf {leaf1.taxon}\n'
          f'  with support {leaf1.parent_node.label}\n'
          f'Second tree leaf {leaf2.taxon}\n'
          f'  with support {leaf2.parent_node.label}')
    sys.exit()

  t1.reroot_at_edge(leaf1.edge)
  t2.reroot_at_edge(leaf2.edge)

  return leaf1, leaf2


def checkPathToRoot(n1: Node, n2: Node, root1: Node, root2: Node) -> bool:
  """
  Return True if both paths to the root (or to the first VISITED edge) have
  the same LEAFSET (or inverse) and have the same support values.
  
  Parameters
  ----------
  n1 : Node
      node from tree 1
  n2 : Node
      node from tree 2
  root1 : Node
      root for tree 1
  root2 : Node
      root for tree 2
  
  Returns
  -------
  bool
      Return True if things match.
  """
  if(n1.visited or n1 == root1 or n2.visited or n2 == root2):
    if(not (n1.visited or n1 == root1) or not (n2.visited or n2 == root2)):
      print(f'Incongruence found between paths to root.\n'
            f'First tree node:\n'
            f'  {n1.leafset}\n'
            f'Second tree node:\n'
            f'  {n2.leafset}')
      return False

    return True   #The end of the upward path.


  if n1.leafset != n2.leafset:
    print(f'Found incongruent edges:\n'
          f'First tree edge:\n'
          f'  {n1.leafset}\n'
          f'Second tree edge:\n'
          f'  {n2.leafset}')
    return False

  #if not n1.is_leaf() and n1.label == None:
  #  print(f'Null internal label at {n1}')
  #if not n2.is_leaf() and n2.label == None:
  #  print(f'Null internal label at {n2}')

  if n1.label != None and n2.label != None and not equalSupport(n1, n2):
    print(f'Found edges with different support:\n'
          f'First tree edge:\n'
          f'  {n1.leafset}\n'
          f'  with support {n1.label}\n'
          f'Second tree edge:\n'
          f'  {n2.leafset}\n'
          f'  with support {n2.label}\n')
    return False

  n1.visited = True
  n2.visited = True

  return checkPathToRoot(n1.parent_node, n2.parent_node, root1, root2)

def equalSupport(n1: Node, n2: Node) -> bool:
  """
  Return True if the support values for these nodes are similar enough.
  
  Parameters
  ----------
  n1 : Node
      first node
  n2 : Node
      second node
  
  Returns
  -------
  bool
      are they within THRESHOLD of each other?
  """
  return abs(float(n1.label) - float(n2.label)) <= THRESHOLD


def labelEdgesWithSupports(tree: Tree) -> None:
  """
  Label nodes with their sets of pendant leaves.
  
  Parameters
  ----------
  tree1 : Tree
      A dendropy Tree object
  """
  for e in tree.postorder_edge_iter():
    if hasattr(e.tail_node, 'label') and e.tail_node.label:
      e.support = e.tail_node.label
    else:
      e.support = 0




#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def main():
  desc = 'Test if the trees are equal, and that their support values match.'
  parser = argparse.ArgumentParser(description=desc)

  parser.add_argument('TREE1',
                      help="a tree file.")
  parser.add_argument('TREE2',
                      help="a tree file.")
  parser.add_argument('-p', action='store_true',
                      help="print the names of the files to compare.")

  args = parser.parse_args()

  treefile1 = args.TREE1
  treefile2 = args.TREE2
  print_names = args.p

  if(not os.path.exists(treefile1)):
    sys.exit(f'ERROR: input tree file {treefile1} missing.')
  if(not os.path.exists(treefile2)):
    sys.exit(f'ERROR: input tree file {treefile2} missing.')

#MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_


  tree1 = Tree.get(path=treefile1, schema='newick', rooting='force-unrooted')

  tree2 = Tree.get(path=treefile2, schema='newick', rooting='force-unrooted',
                   taxon_namespace=tree1.taxon_namespace)

  #labelEdgesWithSupports(tree1)
  #labelEdgesWithSupports(tree2)

  #diffsplits = treecompare.symmetric_difference(tree1, tree2, True)
  #print(f'diff splits: {diffsplits}')
  #diff = treecompare.euclidean_distance(tree1, tree2, 'support')
  #print(f'splits: {diff}')
  

  if print_names:
    print(f'{treefile1} vs. {treefile2}')

  if verifyEqual(tree1, tree2):
    print(f'Trees are equivalent.')



if __name__ == "__main__":
  main()

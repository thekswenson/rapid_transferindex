#!/usr/bin/env python3
import dendropy
import sys
import random
from dendropy.simulate import treesim
import math
import numpy as np
import subprocess
import copy
import argparse

letters=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

#inserts child above a node
def insert_child(child,node):
	newnode=dendropy.Node()
	parent=node.parent_node
	parent.remove_child(node)
	parent.add_child(newnode)
	newnode.add_child(child)
	newnode.add_child(node)
	return child,newnode

#remove child. the child cannot be root or any of its children
def remove_child(child):
	parent=child.parent_node
	grandpa=parent.parent_node
	parent.remove_child(child)
	sibling=parent.child_nodes()[0]
	parent.remove_child(sibling)
	grandpa.remove_child(parent)
	grandpa.add_child(sibling)

def swap_nodes(node1,node2):
	par1=node1.parent_node
	par2=node2.parent_node
	par1.remove_child(node1)
	par2.remove_child(node2)
	par1.add_child(node2)
	par2.add_child(node1)


def check_if_ancestor(node,anc):
	for nd in node.ancestor_iter():
		if nd==anc:
			return True
	return False

def check_mutual_ancestry(node1,node2):
	return (check_if_ancestor(node1,node2) or check_if_ancestor(node2,node1))


def generateNames(nnames):
	length=int(math.ceil(math.log(nnames)/math.log(len(letters))))
	names=[]
	for i in range(nnames):
		x=i
		digits=[]
		for _ in range(length):
			digits.append(x%len(letters))
			x=x/len(letters)
		assert len(digits)==length
		lettered=map(lambda d:letters[d],digits)
		name=''.join(lettered)
		names.append(name)
	return names


def generateDistortedTree(T,nops):
	T2=copy.deepcopy(T)
	t2nodes=[node for node in T2.levelorder_node_iter()]
	for _ in range(nops):
		[node1,node2]=random.sample(t2nodes[1:],2)# don't want root or any of its children
		while check_mutual_ancestry(node1,node2):
			[node1,node2]=random.sample(t2nodes[1:],2)# don't want root or any of its children
		swap_nodes(node1,node2)
		#t2nodes=[node for node in T2.levelorder_node_iter()]
	

	return T2

def generate_tree_pair_kingman(ntaxa,nops):
	taxa=dendropy.TaxonNamespace(["T"+str(i) for i in range(ntaxa)])
	tree=treesim.pure_kingman_tree(taxon_namespace=taxa,pop_size=ntaxa*50)
	tree2=generateDistortedTree(tree,nops)
	return tree,tree2

def splitLeaf(node):
	assert node.is_leaf()
	c1=dendropy.Node()
	c2=dendropy.Node()
	node.add_child(c1)
	node.add_child(c2)
	return c1,c2


def sample_edge_length(mean_len):
	l=np.random.exponential(mean_len)
	return l


def generateUniformTree(nleaves,mean_edge_length):
	root=dendropy.Node()
	c1,c2=splitLeaf(root)
	nodes=[root,c1,c2] # keeps list of current non-root nodes
	leaves=[c1,c2]
	for _ in range(2,nleaves):
		join_point=random.choice(nodes)
		newleaf=dendropy.Node()
		if join_point==root:
			newroot=dendropy.Node()
			newroot.add_child(root)
			newroot.add_child(newleaf)
			root=newroot
			newjoin=newroot
		else:
			newleaf,newjoin=insert_child(newleaf,join_point)
		nodes.append(newleaf)
		nodes.append(newjoin)
		leaves.append(newleaf)

	#assign taxon names
	names=['T'+str(i) for i in range(nleaves)]
	for i in range(len(leaves)):
		leaves[i].taxon=dendropy.Taxon(label=names[i])
	#sample edge lengths
	for node in nodes:
		node.edge_length=sample_edge_length(mean_edge_length)
	T=dendropy.Tree(seed_node=root)
	return T



def generateSpeciesTree(nspecies,mean_edge_length):
	node=dendropy.Node()
	leaves=[node]
	nonleaves={} # hack to do it quickly for large trees
	for _ in range(nspecies-1):
		toSplit=random.randint(0,len(leaves)-1)
		while toSplit in nonleaves:
			toSplit=random.randint(0,len(leaves)-1)
		c1,c2=splitLeaf(leaves[toSplit])
		c1.edge_length=sample_edge_length(mean_edge_length)
		c2.edge_length=sample_edge_length(mean_edge_length)
		#del leaves[toSplit]
		nonleaves[toSplit]=True
		leaves.append(c1)
		leaves.append(c2)

	names=['T'+str(i) for i in range(nspecies)]
	leafids = list(filter(lambda x:x not in nonleaves,range(len(leaves))))
	for i in range(len(leafids)):
		tx=dendropy.Taxon(label=names[i])
		leaves[leafids[i]].taxon=tx
	tree=dendropy.Tree(seed_node=node)
	return tree

def generate_tree_pair_yule(ntaxa,nops):
	tree=generateSpeciesTree(ntaxa,0.02)
	tree2=generateDistortedTree(tree,nops)
	return tree,tree2

def generate_tree_pair_uniform(ntaxa,nops):
	tree=generateUniformTree(ntaxa,0.02)
	tree2=generateDistortedTree(tree,nops)
	return tree,tree2

sys.setrecursionlimit(20000)


desc = 'First generate a Yule tree, and then perturb it to get a second.'
parser = argparse.ArgumentParser(description=desc)

parser.add_argument('NUM_TAXA', type=int,
                    help='the number of taxa')
parser.add_argument('NUM_OPS', type=int,
                    help='the number of perturbations to do on the reference'+
										     ' tree for bootstrap tree generation')
parser.add_argument('NAME_PREFIX',
                    help='the prefix for the output files')
parser.add_argument('-u', '--uniform', action='store_true',
										help='generate uniform rather Yule trees')

args = parser.parse_args()



ntaxa = args.NUM_TAXA
nops = args.NUM_OPS
nameprefix = args.NAME_PREFIX
do_uniform = args.uniform

if do_uniform:
	t1, t2 = generate_tree_pair_uniform(ntaxa, nops)
else:
	t1, t2 = generate_tree_pair_yule(ntaxa, nops)

with open(nameprefix+'_1','w') as f:
	f.write(str(t1)+';')

with open(nameprefix+'_2','w') as f:
	f.write(str(t2)+';')
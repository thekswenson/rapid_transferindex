#!/bin/bash
# changed the output format for the time command
BOOSTER=../../booster/src/booster


for ntaxa in 100 500 1000 5000 10000 50000 100000
do
	for i in `seq 0 9`
	do
		nops=$(echo $ntaxa/5|bc)
		echo $ntaxa
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		echo $prefix'.comp'
		python ../generateTree.py $ntaxa $nops $prefix
		echo 'transfer'
		(/usr/bin/time -v -f "%U" python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp' ) 2> $prefix'.time'
		echo 'booster'
		(/usr/bin/time -v -f "%U" $BOOSTER -a tbe -i $prefix'_1' -b $prefix'_2' > $prefix'.compb' ) 2> $prefix'.timeb'
		
	done
done

#now vary nops

for nops in 0 10 50 100 500 1000 5000 10000 #0 10 50 100 500 1000 5000
do
	for i in `seq 0 9`
	do
		ntaxa=10000
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		echo $nops
		python ../generateTree.py $ntaxa $nops $prefix
		(/usr/bin/time -v -f "%U" python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp') 2> $prefix'.time'
	done
done

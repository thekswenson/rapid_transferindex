#!/bin/bash


for ntaxa in 100 #100000 #100 500 1000 5000 10000 50000 100000
do
	for i in `seq 0 9`
	do
		nops=$(echo $ntaxa/5|bc)
		echo $ntaxa
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		#echo $prefix'.comp'
		python ../generateTree.py $ntaxa $nops $prefix
		#(time python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp' ) 2> $prefix'.time'
		
	done
done

#now vary nops

for nops in 100 500 1000 5000 10000 #0 10 50 #100 500 1000 5000
do
	for i in `seq 0 9`
	do
		ntaxa=10000
		prefix='ntaxa'$ntaxa'nops'$nops't'$i
		echo $nops
		python ../generateTree.py $ntaxa $nops $prefix
		#(time python ../../transfer.py $prefix'_1' $prefix'_2' > $prefix'.comp') 2> $prefix'.time'
	done
done
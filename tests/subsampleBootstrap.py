#!/usr/bin/env python3
import dendropy
import sys
import random
import math
from dendropy.calculate import treecompare

def read_boostrap_file(bpath):
	with open(bpath) as F:
		wholestr=F.read()
		treestraw=wholestr.split(';')
	#treesttrimmed=filter(lambda x:x[0]=='(',map(lambda x:x.strip()+';',treestraw))
	return treestraw

def subsample_bootstrap_file(treestraw,nsamples):
	sub=random.sample(treestraw[:-1],nsamples)
	withsemicolons=map(lambda x:x+';',sub)
	finishedsub=''.join(withsemicolons)#+treestraw[-1]
	return finishedsub.lstrip() #without the final \n 





if len(sys.argv)<3:
	sys.stderr.write("Usage: "+sys.argv[0]+" original_bootstrap_file nsamples\n")
	exit(0)
nsamples=int(sys.argv[2])
treestraw=read_boostrap_file(sys.argv[1])
newfile=subsample_bootstrap_file(treestraw,nsamples)
print(newfile)

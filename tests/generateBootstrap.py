#!/usr/bin/env python3

import os
import argparse
import multiprocessing
import dendropy
import sys
import random
from dendropy.simulate import treesim
import math
import numpy as np
import subprocess
import copy

sys.setrecursionlimit(40000)  #For working with BIG trees.


letters=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

#inserts child above a node
def insert_child(child,node):
	newnode=dendropy.Node()
	parent=node.parent_node
	parent.remove_child(node)
	parent.add_child(newnode)
	newnode.add_child(child)
	newnode.add_child(node)

#remove child. the child cannot be root or any of its children
def remove_child(child):
	parent=child.parent_node
	grandpa=parent.parent_node
	parent.remove_child(child)
	sibling=parent.child_nodes()[0]
	parent.remove_child(sibling)
	grandpa.remove_child(parent)
	grandpa.add_child(sibling)

def swap_nodes(node1,node2):
	par1=node1.parent_node
	par2=node2.parent_node
	par1.remove_child(node1)
	par2.remove_child(node2)
	par1.add_child(node2)
	par2.add_child(node1)


def check_if_ancestor(node,anc):
	for nd in node.ancestor_iter():
		if nd==anc:
			return True
	return False

def check_mutual_ancestry(node1,node2):
	return (check_if_ancestor(node1,node2) or check_if_ancestor(node2,node1))


def generateNames(nnames):
	length=int(math.ceil(math.log(nnames)/math.log(len(letters))))
	names=[]
	for i in range(nnames):
		x=i
		digits=[]
		for _ in range(length):
			digits.append(x%len(letters))
			x=x/len(letters)
		assert len(digits)==length
		lettered=map(lambda d:letters[d],digits)
		name=''.join(lettered)
		names.append(name)
	return names


def generateDistortedTree(T,nops):
	T2 = dendropy.Tree(T)
	#T2 = T
	#swaplist = []
	t2nodes=[node for node in T2.levelorder_node_iter()]
	for _ in range(nops):
		[node1,node2]=random.sample(t2nodes[1:],2)# don't want root or any of its children
		while check_mutual_ancestry(node1,node2):
			[node1,node2]=random.sample(t2nodes[1:],2)# don't want root or any of its children
		swap_nodes(node1,node2)
		#swaplist.append((node1, node2))

	#retnewick = str(T2)
	#for node1, node2 in reversed(swaplist):
	#	swap_nodes(node1, node2)

	return T2

def generate_tree_pair_kingman(ntaxa,nops):
	taxa=dendropy.TaxonNamespace(["T"+str(i) for i in range(ntaxa)])
	tree=treesim.pure_kingman_tree(taxon_namespace=taxa,pop_size=ntaxa*50)
	tree2=generateDistortedTree(tree,nops)
	return tree,tree2

def splitLeaf(node):
	assert node.is_leaf()
	c1=dendropy.Node()
	c2=dendropy.Node()
	node.add_child(c1)
	node.add_child(c2)
	return c1,c2


def sample_edge_length(mean_len):
	l=np.random.exponential(mean_len)
	return l

def generateSpeciesTree(nspecies,mean_edge_length):
	node=dendropy.Node()
	leaves=[node]
	nonleaves={} # hack to do it quickly for large trees
	for _ in range(nspecies-1):
		toSplit=random.randint(0,len(leaves)-1)
		while toSplit in nonleaves:
			toSplit=random.randint(0,len(leaves)-1)
		c1,c2=splitLeaf(leaves[toSplit])
		c1.edge_length=sample_edge_length(mean_edge_length)
		c2.edge_length=sample_edge_length(mean_edge_length)
		#del leaves[toSplit]
		nonleaves[toSplit]=True
		leaves.append(c1)
		leaves.append(c2)

	names=['T'+str(i) for i in range(nspecies)]
	leafids = list(filter(lambda x:x not in nonleaves,range(len(leaves))))
	for i in range(len(leafids)):
		tx=dendropy.Taxon(label=names[i])
		leaves[leafids[i]].taxon=tx
	tree=dendropy.Tree(seed_node=node)
	return tree

def generate_tree_pair_yule(ntaxa,nops):
	tree=generateSpeciesTree(ntaxa,0.02)
	tree2=generateDistortedTree(tree,nops)
	return tree,tree2

def generate_bootstrap(ntaxa, nops, nboot, treefile='', bootfile='',
                       numprocessors=1):
	tree = generateSpeciesTree(ntaxa, 0.02)

	if treefile:
		with open(treefile, 'w') as f:
 			f.write('{};\n'.format(tree))

	boots=[]
	if bootfile:
		if numprocessors > 1:
		  pool = multiprocessing.Pool(numprocessors)
		  result = pool.map(distortedTreeToFile_unpack,
			                  ((tree, nops, bootfile) for _ in range(nboot)))

		else:
			for i in range(nboot):
				print("generating tree:"+str(i))
				distortedTreeToFile(tree, nops, bootfile)

	else:
		for i in range(nboot):
			print("generating tree:"+str(i))
			tree2 = generateDistortedTree(tree, nops)
			boots.append(tree2)

	return tree,boots
	

def distortedTreeToFile_unpack(args):
	distortedTreeToFile(*args)

def distortedTreeToFile(tree, nops, filename):
	try:
		tree2 = generateDistortedTree(tree, nops)
	except RuntimeError:
		distortedTreeToFile(tree, nops, filename)
	else:
	  with open(filename, 'a') as f:
		  f.write('{};\n'.format(tree2))
	

desc = 'Generate Yule trees with bootstrap sets.'
parser = argparse.ArgumentParser(description=desc)

parser.add_argument('NUM_TAXA', type=int,
                    help='the number of taxa')
parser.add_argument('NUM_OPS', type=int,
                    help='the number of perturbations to do on the reference'+
										     ' tree for bootstrap tree generation')
parser.add_argument('NUM_BOOTSTRAPS', type=int,
                    help='the number of replicates')
parser.add_argument('NAME_PREFIX',
                    help='the prefix for the output files')
parser.add_argument('-p', '--parallel', metavar='NUM_PROCESSORS', type=int,
										default=1, help='use this many processors')

args = parser.parse_args()


ntaxa = args.NUM_TAXA
nops = args.NUM_OPS
nboot = args.NUM_BOOTSTRAPS
nameprefix = args.NAME_PREFIX
num_processors = args.parallel

treefile = nameprefix+'_ref.nw'
bootfile = nameprefix+'_boot.nw'
if os.path.exists(treefile):
	sys.exit('ERROR: treefile "{}" exits!'.format(treefile))
if os.path.exists(bootfile):
	sys.exit('ERROR: bootfile "{}" exits!'.format(bootfile))

t1, bootsample = generate_bootstrap(ntaxa, nops, nboot, treefile, bootfile,
																		num_processors)

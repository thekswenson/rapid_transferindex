#!/usr/bin/env python3
import dendropy
import sys
import random
import math
from dendropy.calculate import treecompare

def annotate_tree_with_bp(tree):
	for node in tree.postorder_node_iter():
		if node.is_leaf() or node.parent_node is None:
			continue
		node.bp=float(node.label)

if len(sys.argv)<3:
	print("Usage: "+sys.argv[0]+" tree1 tree2\n", file=sys.stderr)
	exit(0)

tree1=dendropy.Tree.get_from_path(sys.argv[1],schema='newick')
tree2=dendropy.Tree.get_from_path(sys.argv[2],schema='newick',taxon_namespace=tree1.taxon_namespace)
splits=treecompare.symmetric_difference(tree1,tree2)
diff=treecompare.euclidean_distance(tree1,tree2)


print(str(splits)+' '+str(diff))

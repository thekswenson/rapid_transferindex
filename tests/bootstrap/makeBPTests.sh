#!/bin/bash

BOOSTERPATH=~/booster/booster/src/booster
TRANSFERPATH=~/transferfast/transferindex/transfer.py
GENBOOT=~/transferfast/transferindex/tests/generateBootstrap.py
COMPARE=~/transferfast/transferindex/tests/compareScores.py

for ntaxa in 50 100 200
do
	for i in `seq 0 4`
	do
		nops=10
		nboot=100
		prefix='ntaxa'$ntaxa'nops'$nops'nboot'$nboot't'$i
		#echo $prefix'.comp'
		python $GENBOOT $ntaxa $nops $nboot $prefix
		python $TRANSFERPATH $prefix'.tree' $prefix'.boot' boot > $prefix'.tbe'
		$BOOSTERPATH -i $prefix'.tree' -b $prefix'.boot' > $prefix'.booster'
		python $COMPARE $prefix'.tbe' $prefix'.booster' > $prefix'.comp'
		cat $prefix'.comp'
		#if cmp $prefix'.comp' $prefix'.compnaive' 
		#then
		#	echo 'test:'$prefix': OK'
		#else
		#	echo 'test:'$prefix': WRONG!!!'
		#fi
	done
done

ntaxa=100
nboot=100

for nops in 50 80
do
	for i in `seq 0 4`
	do
		#nops=10
		#nboot=100
		prefix='ntaxa'$ntaxa'nops'$nops'nboot'$nboot't'$i
		#echo $prefix'.comp'
		python $GENBOOT $ntaxa $nops $nboot $prefix
		python $TRANSFERPATH $prefix'.tree' $prefix'.boot' boot > $prefix'.tbe'
		$BOOSTERPATH -i $prefix'.tree' -b $prefix'.boot' > $prefix'.booster'
		python $COMPARE $prefix'.tbe' $prefix'.booster' > $prefix'.comp'
		cat $prefix'.comp'
		#if cmp $prefix'.comp' $prefix'.compnaive' 
		#then
		#	echo 'test:'$prefix': OK'
		#else
		#	echo 'test:'$prefix': WRONG!!!'
		#fi
	done
done

ntaxa=100
nops=20

for nboot in 333 1000
do
	for i in `seq 0 4`
	do
		#nops=10
		#nboot=100
		prefix='ntaxa'$ntaxa'nops'$nops'nboot'$nboot't'$i
		#echo $prefix'.comp'
		python $GENBOOT $ntaxa $nops $nboot $prefix
		python $TRANSFERPATH $prefix'.tree' $prefix'.boot' boot > $prefix'.tbe'
		$BOOSTERPATH -i $prefix'.tree' -b $prefix'.boot' > $prefix'.booster'
		python $COMPARE $prefix'.tbe' $prefix'.booster' > $prefix'.comp'
		cat $prefix'.comp'
		#if cmp $prefix'.comp' $prefix'.compnaive' 
		#then
		#	echo 'test:'$prefix': OK'
		#else
		#	echo 'test:'$prefix': WRONG!!!'
		#fi
	done
done

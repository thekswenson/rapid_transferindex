#!/bin/bash

BOOSTERPATH=~/booster/booster/src/booster
TRANSFERPATH=~/transferfast/transferindex/transfer.py
GENBOOT=~/transferfast/transferindex/tests/generateBootstrap.py
COMPARE=~/transferfast/transferindex/tests/compareScores.py
SUBSAMPLE=~/transferfast/transferindex/tests/subsampleBootstrap.py

BOOTFILE=~/booster/booster/examples/boot.nw
REFFILE=~/booster/booster/examples/ref.nw

for nboot in 50 200 500
do
	for i in `seq 0 4`
	do
		
		prefix=$REFFILE'nboot'$nboot't'$i
		python $SUBSAMPLE $BOOTFILE $nboot > $prefix'.boot'
		#echo $prefix'.comp'
		#python $GENBOOT $ntaxa $nops $nboot $prefix
		python $TRANSFERPATH $REFFILE $prefix'.boot' boot > $prefix'.tbe'
		$BOOSTERPATH -i $REFFILE -b $prefix'.boot' > $prefix'.booster'
		python $COMPARE $prefix'.tbe' $prefix'.booster' > $prefix'.comp'
		cat $prefix'.comp'
		#if cmp $prefix'.comp' $prefix'.compnaive' 
		#then
		#	echo 'test:'$prefix': OK'
		#else
		#	echo 'test:'$prefix': WRONG!!!'
		#fi
	done
done




